CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers


INTRODUCTION
------------

The Getty Images download module provides UI to configure
Getty image API and widget to search getty images and download
the selected images from Getty server and make avaliable under
the drupal media entity for feture use.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/getty_images_download

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/getty_images_download

REQUIREMENTS
------------

This module requires the following modules:

 * Views (https://www.drupal.org/project/media)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------
 
 * Configure the Getty image API credentials visit to
 Administration » Configuration » System »
 Getty Images Server configuration :
    http://{hostname}/admin/config/system/gettyimageserver
   
TROUBLESHOOTING
---------------

 * If have any issue to search and download images, check the following:

   - Please navigate Administration > Report > Recent log Messages

FAQ
---
Q: How can I create Getty image server account?

A: Please visit http://engage.gettyimages.com/api-contact link get more details.

Q: If Getty image server credentials are not avaliable,
will I able to search and download the images?

A: No, to search and download images from getty image
server need a API credentails.

MAINTAINERS
-----------

Current maintainers:
 * Mahesh Patil - https://www.drupal.org/user/54136

This project has been sponsored by:
 * iHorizons Media Pvt. Ltd.
