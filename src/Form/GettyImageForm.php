<?php

namespace Drupal\getty_images_download\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\media\Entity\Media;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GuzzleHttp\Client;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Getty image search form by FormBase class. */
class GettyImageForm extends FormBase {
  /**
   * Guzzle\Client instance.
   *
   * @var \Guzzle\Client
   */
  protected $httpClient;
  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * This is account variable.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;
  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */

  protected $configFactory;
  /**
   * This is server configuration.
   *
   * @var string
   */
  protected $gettyimagesSeverConfig;
  /**
   * This is store the tokendetails.
   *
   * @var string
   */
  protected $tokenDetails;

  /**
   * Constructs a new CustomService object.
   */
  public function __construct(Client $http_client, RendererInterface $renderer, EntityTypeManagerInterface $entity_type_manager, AccountInterface $account, ConfigFactoryInterface $config_factory) {
    $this->httpClient = $http_client;
    $this->renderer = $renderer;
    $this->entityTypeManager = $entity_type_manager;
    $this->account = $account;
    $this->gettyimagesSeverConfig = $config_factory->get('getty_images_download.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client'),
      $container->get('renderer'),
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'modal_form_gettyimage_search';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $options = NULL) {
    $form['description'] = [
      '#type' => 'item',
      '#markup' => $this->t('Search and download images from GettyImages.com'),
    ];
    $form['#tree'] = TRUE;
    $form['wrapper'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'data-wrapper'],
    ];
    $form['wrapper']['searchinput'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Search'),
      '#attributes' => [
        'class' => ['form--inline clearfix'],
      ],
    ];
    $form['wrapper']['searchinput']['searchtext'] = [
      '#type' => 'textfield',
      '#size' => '30',
      '#title' => $this->t('Search Keyword!'),
    ];
    $form['wrapper']['searchinput']['sortby'] = [
      '#type' => 'select',
      '#title' => $this->t('Sort By'),
      '#default_value' => 'newest',
      '#options' => [
        "best_match" => $this->t("Best Match"),
        "most_popular" => $this->t("Most Popular"),
        "newest" => $this->t("Newest"),
        "random" => $this->t("Random"),
      ],
    ];
    $currentPage = 1;
    if (!empty($form_state->get('currentPage'))) {
      $currentPage = $form_state->get('currentPage');
    }
    $form['wrapper']['searchinput']['currentPage'] = [
      '#type' => 'textfield',
      '#size' => 10,
      '#title' => $this->t('Current Page'),
      '#value' => $currentPage,
      '#attributes' => ['readonly' => 'readonly'],
    ];
    $form['wrapper']['searchinput']['perPageRec'] = [
      '#type' => 'select',
      '#title' => $this->t('Record per Page'),
      '#options' => [10 => 10, 50 => 50, 75 => 75, 100 => 100],
    ];

    $form['wrapper']['searchinput']['actions'] = [
      '#type' => 'actions',
      '#attributes' => [
        'style' => ['display:inline'],
      ],
    ];
    $form['wrapper']['searchinput']['actions']['search'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search'),
      '#submit' => ['::searchGettyImages'],
      '#ajax' => [
        'callback' => '::searchGettyImagesCallback',
        'wrapper' => 'data-wrapper',
      ],
    ];
    $form['wrapper']['searchresult'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Result Found'),
      '#attributes' => [
        'class' => ['form--inline clearfix'],
      ],
    ];

    if (!empty($form_state->get('resultCount'))) {
      $form['wrapper']['searchresult']['top']['actions']['download'] = [
        '#type' => 'submit',
        '#value' => $this->t('Download'),
        '#submit' => ['::downloadGettyImages'],
        '#attributes' => [
          'class' => ['download-gettyimage'],
        ],
        '#ajax' => [
          'callback' => '::searchGettyImagesCallback',
          'wrapper' => 'data-wrapper',
        ],
      ];
    }
    // Gather the number of names in the form already.
    $imageResult = $form_state->get('imageResult');
    // We have to ensure that there is at least one name field.
    if ($imageResult === NULL) {
      $imageResult = [];
    }

    $header = [
      'title' => $this->t("Title"),
      'sizes' => $this->t('Sizes'),
      "image" => $this->t("Thumbnail"),
      "is_downloaded" => $this->t("Downloaded"),
      'date_created' => $this->t("Created"),
      'asset_family' => $this->t("Assest Family"),
      'caption' => $this->t("Caption"),
      'copyright' => $this->t("Copyright"),
    ];
    $form['wrapper']['searchresult']['imageslist'] = [
      '#id' => 'select-imageslist',
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $imageResult,
      '#multiple' => "FALSE",
      '#js_select' => "FALSE",
      '#empty' => $this->t('No image found'),
    ];
    if (!empty($form_state->get('resultCount'))) {

      if ($currentPage > 1) {
        $form['wrapper']['searchresult']['bottom']['actions']['previous'] = [
          '#type' => 'submit',
          '#value' => $this->t('Previous'),
          '#attributes' => [
            'class' => ['prev-gettyimage'],
          ],
          '#submit' => ['::searchPreviousGettyImages'],
          '#ajax' => [
            'callback' => '::searchGettyImagesCallback',
            'wrapper' => 'data-wrapper',
          ],
        ];
      }
      $form['wrapper']['searchresult']['bottom']['actions']['next'] = [
        '#type' => 'submit',
        '#value' => $this->t('Next'),
        '#attributes' => [
          'class' => ['next-gettyimage'],
        ],
        '#submit' => ['::searchNextGettyImages'],
        '#ajax' => [
          'callback' => '::searchGettyImagesCallback',
          'wrapper' => 'data-wrapper',
        ],
      ];
    }

    $form['wrapper']['searchresult']['sizes'] = [
      '#type' => 'value',
    ];

    $form['#attached']['library'][] = 'getty_images_download/getty_images_download_css';

    return $form;
  }

  /**
   * Callback for both ajax-enabled buttons.
   *
   * Selects and returns the fieldset with the names in it.
   */
  public function searchGettyImagesCallback(array &$form, FormStateInterface $form_state) {
    return $form['wrapper'];
  }

  /**
   * Submit handler for the "Search" button.
   *
   * Increments the set the token value and rebuild.
   */
  public function searchGettyImages(array &$form, FormStateInterface $form_state) {

    $values = $form_state->getValues();
    $searchText = trim($values['wrapper']['searchinput']['searchtext']);
    $sortBy = trim($values['wrapper']['searchinput']['sortby']);
    $messenger = $this->messenger();
    try {
      if (empty($form_state->get('token'))) {

        $tokenDetails = $this->getAuthToken();
        $tokenDetailsObj = json_decode($tokenDetails);
        $form_state->set('token', $tokenDetailsObj->token_type . ' ' . $tokenDetailsObj->access_token);
        $nextPage = $form_state->getValue(['wrapper', 'searchinput'])['currentPage'];
      }
      else {
        $nextPage = 1;
      }
      $perPageRec = $form_state->getValue(['wrapper', 'searchinput'])['perPageRec'];
      $responseInfo = $this->searchApi($nextPage, $perPageRec, $searchText, $sortBy, $form_state->get('token'));
      $form_state->set('resultCount', $responseInfo['totalMatch']);
      $form_state->set('imageResult', $responseInfo['imageResult']);
      $form_state->set('currentPage', $nextPage);

      $messenger->addMessage($this->t('Matching images are: @imgCount', ['@imgCount' => $form_state->get('resultCount')]));
    }
    catch (Exception $e) {
      watchdog_exception('GettyImageException', $e);
      $messenger->addMessage($this->t('Something is wrong...Please try again!!'), 'error', TRUE);
    }

    $form_state->setRebuild();
  }

  /**
   * The function are used to navigate to next images.
   */
  public function searchNextGettyImages(array &$form, FormStateInterface $form_state) {

    $values = $form_state->getValues();
    $searchText = trim($values['wrapper']['searchinput']['searchtext']);
    $sortBy = trim($values['wrapper']['searchinput']['sortby']);
    $messenger = $this->messenger();
    try {

      if (!empty($form_state->get('token'))) {
        $nextPage = $form_state->getValue(['wrapper', 'searchinput'])['currentPage'] + 1;
        $perPageRec = $form_state->getValue(['wrapper', 'searchinput'])['perPageRec'];
        $responseInfo = $this->searchApi($nextPage, $perPageRec, $searchText, $sortBy, $form_state->get('token'));
        $form_state->set('resultCount', $responseInfo['totalMatch']);
        $form_state->set('imageResult', $responseInfo['imageResult']);
        $form_state->set('currentPage', $nextPage);
        $messenger->addMessage($this->t('You got the total: @imgCount', ['@imgCount' => $form_state->get('resultCount')]));
      }

    }
    catch (\Exception $e) {
      watchdog_exception('GettyImageException', $e);
      $messenger->addMessage($this->t('Something is wrong...Please try again!!'), 'error', TRUE);
    }

    $form_state->setRebuild();
  }

  /**
   * The function are used to navigate to previous page.
   */
  public function searchPreviousGettyImages(array &$form, FormStateInterface $form_state) {

    $values = $form_state->getValues();
    $searchText = trim($values['wrapper']['searchinput']['searchtext']);
    $sortBy = trim($values['wrapper']['searchinput']['sortby']);
    $messenger = $this->messenger();
    try {

      if (!empty($form_state->get('token'))) {
        $nextPage = $form_state->getValue(['wrapper', 'searchinput'])['currentPage'] - 1;
        $perPageRec = $form_state->getValue(['wrapper', 'searchinput'])['perPageRec'];
        $responseInfo = $this->searchApi($nextPage, $perPageRec, $searchText, $sortBy, $form_state->get('token'));
        $form_state->set('resultCount', $responseInfo['totalMatch']);
        $form_state->set('imageResult', $responseInfo['imageResult']);
        $form_state->set('currentPage', $nextPage);
        $messenger->addMessage($this->t('You got the total: @imgCount', ['@imgCount' => $form_state->get('resultCount')]));
      }

    }
    catch (\Exception $e) {
      watchdog_exception('GettyImageException', $e);
      $messenger->addMessage($this->t('Something is wrong...Please try again!!'), 'error', TRUE);
    }

    $form_state->setRebuild();
  }

  /**
   * This function used to search the image with selected filter.
   */
  protected function searchApi($nextPage, $perPageRec, $searchText, $sortBy, $token) {
    $responseInfo = [];
    $imagesJson = $this->httpClient->get($this->gettyimagesSeverConfig->get('baseUrl') . "$nextPage&page_size=$perPageRec&phrase=$searchText&sort_order=$sortBy", [
      'verify' => FALSE,
      'headers' => [
        'accept' => 'text/plain',
        'Api-Key' => $this->gettyimagesSeverConfig->get('key'),
        'Content-type' => 'application/x-www-form-urlencoded',
        'Authorization' => $token,
      ],
    ])->getBody()->getContents();
    $responseImageSearch = json_decode($imagesJson, TRUE);
    (isset($responseImageSearch['result_count'])) ? $responseInfo['totalMatch'] = $responseImageSearch['result_count'] : 0;
    $imageResult = [];
    if (isset($responseImageSearch['images']) && count($responseImageSearch['images']) > 0) {

      foreach ($responseImageSearch['images'] as $key => $imageInfo) {
        $imageResult[$imageInfo['id']] = $imageInfo;
        $image_variables = [
          '#theme' => 'image',
          '#uri' => $imageInfo['display_sizes'][0]['uri'],
          '#alt' => $imageInfo['title'],
          '#title' => $imageInfo['title'],
          '#prefix' => '<a class="use-ajax" href="' . $imageInfo['display_sizes'][0]['uri'] . '" data-group="' . $key . '">',
          '#suffix' => "</a>",
        ];

        $thumb = $this->renderer->render($image_variables);
        $imageResult[$imageInfo['id']]['image'] = $thumb;
        $dateCreated = explode("T", $imageInfo['date_created']);
        $imageResult[$imageInfo['id']]['date_created'] = $dateCreated[0];
        $uri = "public://" . $imageInfo['id'] . ".jpg";
        $fileExist = $this->entityTypeManager->getStorage('file')->loadByProperties(['uri' => $uri]);
        if (empty($fileExist)) {
          $uri = "public://" . $imageInfo['id'] . ".jpeg";
          $fileExist = $this->entityTypeManager->getStorage('file')->loadByProperties(['uri' => $uri]);
        }
        $imageResult[$imageInfo['id']]['is_downloaded'] = (!empty($fileExist)) ? "Yes" : "No";
        $sizes = [];
        foreach ($imageInfo['download_sizes'] as $key => $value) {
          $kb = ($value['bytes'] / 1000);
          $sizes[$value['height']] = $value['width'] . "x" . $value['height'] . "($kb KB)";
        }

        $imageResult[$imageInfo['id']]['sizes'] = [
          'data' => [
            '#id' => 'sizes-' . $imageInfo['id'],
            '#type' => 'select',
            '#title' => 'Download Sizes',
            '#options' => $sizes,
            '#name' => "wrapper[searchresult][sizes][" . $imageInfo['id'] . "]",
          ],
        ];
      }
    }
    $responseInfo['imageResult'] = $imageResult;
    return $responseInfo;
  }

  /**
   * This functiona are used to download the selected getty images.
   */
  public function downloadGettyImages(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $messenger = $this->messenger();
    $downloadImg = array_filter($values['wrapper']['searchresult']['imageslist']);
    $imageSize = array_filter($values['wrapper']['searchresult']['sizes']);
    if (!empty($downloadImg) && count($downloadImg) > 0 && !empty($imageSize) && count($imageSize) > 0) {
      foreach ($downloadImg as $key => $imageId) {
        $height = $imageSize[$imageId];
        $downloadRes = $this->httpClient->post("https://api.gettyimages.com/v3/downloads/images/$imageId?auto_download=true&height=$height&file_type=jpg", [
          'verify' => FALSE,
          'headers' => [
            'accept' => 'text/plain',
            'Api-Key' => $this->gettyimagesSeverConfig->get('key'),
            'Content-type' => 'application/x-www-form-urlencoded',
            'Authorization' => $form_state->get('token'),
          ],
        ]);
        $imageData = $downloadRes->getBody()->getContents();
        $imageType = $downloadRes->getHeader('content-type')[0];
        $imgExt = explode("/", $imageType)[1];
        $file = file_save_data($imageData, "public://$imageId.$imgExt", FILE_EXISTS_REPLACE);
        if (!empty($file)) {
          $imgDetails = $this->httpClient->get("https://api.gettyimages.com/v3/images/$imageId", [
            'verify' => FALSE,
            'headers' => [
              'accept' => 'text/plain',
              'Api-Key' => $this->gettyimagesSeverConfig->get('key'),
              'Content-type' => 'application/x-www-form-urlencoded',
              'Authorization' => $form_state->get('token'),
            ],
          ]);
          $imageDetailJson = json_decode($imgDetails->getBody()->getContents(), TRUE);
          if (isset($imageDetailJson['images']) && count($imageDetailJson['images']) > 0) {
            foreach ($imageDetailJson['images'] as $key => $imgInfo) {
              $media_entity = Media::create([
                'bundle' => 'image',
                'uid' => $this->account->id(),
                'name' => substr($imgInfo['title'], 0, 68),
                'field_media_image' => [
                  'target_id' => $file->id(),
                  'alt' => '',
                ],
              ]);
              $media_entity->save();
            }
          }
        }
      }
      $messenger->addMessage($this->t('Selected image downloaded successfully.'));
    }
    else {
      $messenger->addMessage($this->t('Please select image and size to download.'), 'error', TRUE);
    }
    $form_state->setRebuild();
  }

  /**
   * This function to auth and get a token.
   */
  protected function getAuthToken() {

    try {
      $tokenDetails = $this->httpClient->post($this->gettyimagesSeverConfig->get('authUrl'), [
        'verify' => FALSE,
        'form_params' => [
          'grant_type' => 'password',
          'client_id' => $this->gettyimagesSeverConfig->get('key'),
          'client_secret' => $this->gettyimagesSeverConfig->get('secret'),
          'scope' => '',
          'username' => $this->gettyimagesSeverConfig->get('username'),
          'password' => $this->gettyimagesSeverConfig->get('password'),
        ],
        'headers' => [
          'Content-type' => 'application/x-www-form-urlencoded',
        ],
      ])->getBody()->getContents();
      return $tokenDetails;
    }
    catch (Exception $e) {
      watchdog_exception('GettyImageException1', $e);
      throw new Exception("Error Processing Request", 1);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

}
