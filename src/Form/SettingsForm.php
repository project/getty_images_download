<?php

namespace Drupal\getty_images_download\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure gettyimages settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'gettyimages_server_config';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['getty_images_download.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['baseUrl'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Enter API Base URL'),
      '#default_value' => $this->config('getty_images_download.settings')->get('baseUrl'),
    ];
    $form['authUrl'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter API Auth Url'),
      '#default_value' => $this->config('getty_images_download.settings')->get('authUrl'),
    ];
    $form['key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter API Key OR client_id:'),
      '#default_value' => $this->config('getty_images_download.settings')->get('key'),
    ];
    $form['secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter Client secret:'),
      '#default_value' => $this->config('getty_images_download.settings')->get('secret'),
    ];
    $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter Username:'),
      '#default_value' => $this->config('getty_images_download.settings')->get('username'),
    ];
    $form['password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter Password:'),
      '#default_value' => $this->config('getty_images_download.settings')->get('password'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (empty($form_state->getValue('baseUrl'))) {
      $form_state->setErrorByName('baseUrl', $this->t('The API baseUrl value should not be empty.'));
    }
    if (empty($form_state->getValue('authUrl'))) {
      $form_state->setErrorByName('authUrl', $this->t('The API authUrl value should not be empty.'));
    }
    if (empty($form_state->getValue('key'))) {
      $form_state->setErrorByName('key', $this->t('The API Key value should not be empty.'));
    }
    if (empty($form_state->getValue('secret'))) {
      $form_state->setErrorByName('secret', $this->t('The API secret value should not be empty.'));
    }
    if (empty($form_state->getValue('username'))) {
      $form_state->setErrorByName('username', $this->t('The API username value should not be empty.'));
    }
    if (empty($form_state->getValue('password'))) {
      $form_state->setErrorByName('password', $this->t('The API Use Password value should not be empty.'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('getty_images_download.settings')
      ->set('baseUrl', $form_state->getValue('baseUrl'))
      ->set('authUrl', $form_state->getValue('authUrl'))
      ->set('key', $form_state->getValue('key'))
      ->set('secret', $form_state->getValue('secret'))
      ->set('username', $form_state->getValue('username'))
      ->set('password', $form_state->getValue('password'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
